EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "39 pos data pigtail for rd53b_quad hybrid design"
Date "2020-10-21"
Rev "1.1"
Comp "LBNL"
Comment1 "Aleksandra Dimitrievska"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pigtail_39pos:Pigtail39Pos U1
U 1 1 5F8EBBD5
P 2900 1900
F 0 "U1" H 2783 2465 50  0000 C CNN
F 1 "Pigtail39Pos" H 2783 2374 50  0000 C CNN
F 2 "Library:Pigtail_39Pos" H 2900 1900 50  0001 C CNN
F 3 "" H 2900 1900 50  0001 C CNN
	1    2900 1900
	1    0    0    -1  
$EndComp
$Comp
L pigtail_39pos:Pigtail39Pos U2
U 1 1 5F8EDE31
P 5550 5100
F 0 "U2" H 5072 3454 50  0000 R CNN
F 1 "Pigtail39Pos" H 5072 3545 50  0000 R CNN
F 2 "Library:Pigtail_39Pos" H 5550 5100 50  0001 C CNN
F 3 "" H 5550 5100 50  0001 C CNN
	1    5550 5100
	-1   0    0    1   
$EndComp
Wire Wire Line
	3100 1600 5350 1600
Wire Wire Line
	3100 1700 5350 1700
Wire Wire Line
	3100 1800 5350 1800
Wire Wire Line
	3100 1900 5350 1900
Wire Wire Line
	3100 2000 5350 2000
Wire Wire Line
	3100 2100 5350 2100
Wire Wire Line
	3100 2200 5350 2200
Wire Wire Line
	3100 2300 5350 2300
Wire Wire Line
	3100 2400 5350 2400
Wire Wire Line
	3100 2500 5350 2500
Wire Wire Line
	3100 2600 5350 2600
Wire Wire Line
	3100 2700 5350 2700
Wire Wire Line
	3100 2800 5350 2800
Wire Wire Line
	3100 2900 5350 2900
Wire Wire Line
	3100 3000 5350 3000
Wire Wire Line
	3100 3100 5350 3100
Wire Wire Line
	3100 3200 5350 3200
Wire Wire Line
	3100 3300 5350 3300
Wire Wire Line
	3100 3400 5350 3400
Wire Wire Line
	3100 3500 5350 3500
Wire Wire Line
	3100 3600 5350 3600
Wire Wire Line
	3100 3700 5350 3700
Wire Wire Line
	3100 3800 5350 3800
Wire Wire Line
	3100 3900 5350 3900
Wire Wire Line
	3100 4000 5350 4000
Wire Wire Line
	3100 4100 5350 4100
Wire Wire Line
	3100 4200 5350 4200
Wire Wire Line
	3100 4300 5350 4300
Wire Wire Line
	3100 4400 5350 4400
Wire Wire Line
	3100 4500 5350 4500
Wire Wire Line
	3100 4600 5350 4600
Wire Wire Line
	3100 4700 5350 4700
Wire Wire Line
	3100 4800 5350 4800
Wire Wire Line
	3100 4900 5350 4900
Wire Wire Line
	3100 5000 5350 5000
Wire Wire Line
	3100 5100 5350 5100
Wire Wire Line
	3100 5200 5350 5200
Wire Wire Line
	3100 5300 5350 5300
Wire Wire Line
	3100 5400 5350 5400
Text Label 4000 1600 0    50   ~ 0
MUX_Chip2
Text Label 4000 1700 0    50   ~ 0
LVDS0_Chip1_N
Text Label 4000 1800 0    50   ~ 0
LVDS0_Chip1_P
Text Label 4000 1900 0    50   ~ 0
GND
Text Label 4000 2000 0    50   ~ 0
GTX0_Chip2_N
Text Label 4000 2100 0    50   ~ 0
GTX0_Chip2_P
Text Label 4000 2200 0    50   ~ 0
GND
Text Label 4000 2300 0    50   ~ 0
GTX3_Chip1_P
Text Label 4000 2400 0    50   ~ 0
GTX3_Chip1_N
Text Label 4000 2500 0    50   ~ 0
GND
Text Label 4000 2600 0    50   ~ 0
GTX1_Chip2_N
Text Label 4000 2700 0    50   ~ 0
GTX1_Chip2_P
Text Label 4000 2800 0    50   ~ 0
GND
Text Label 4000 2900 0    50   ~ 0
GTX2_Chip1_P
Text Label 4000 3000 0    50   ~ 0
GTX2_Chip1_N
Text Label 4000 3100 0    50   ~ 0
GND_C
Text Label 4000 3200 0    50   ~ 0
CHIP_ID_P
Text Label 4000 3300 0    50   ~ 0
CHIP_ID_N
Text Label 4000 3400 0    50   ~ 0
LP_EN
Text Label 4000 3500 0    50   ~ 0
MUX_Chip1
Text Label 4000 3700 0    50   ~ 0
GTX2_Chip3_N
Text Label 4000 3800 0    50   ~ 0
GTX2_Chip3_P
Text Label 4000 3900 0    50   ~ 0
GND_S
Text Label 4000 4000 0    50   ~ 0
GTX1_Chip4_P
Text Label 4000 4100 0    50   ~ 0
GTX1_Chip4_N
Text Label 4000 4200 0    50   ~ 0
GND
Text Label 4000 4300 0    50   ~ 0
GTX3_Chip3_N
Text Label 4000 4400 0    50   ~ 0
GTX3_Chip3_P
Text Label 4000 4500 0    50   ~ 0
GND
Text Label 4000 4600 0    50   ~ 0
GTX0_Chip4_P
Text Label 4000 4700 0    50   ~ 0
GTX0_Chip4_N
Text Label 4000 3600 0    50   ~ 0
MUX_Chip3
Text Label 4000 4800 0    50   ~ 0
GND
Text Label 4000 4900 0    50   ~ 0
CMD_N
Text Label 4000 5000 0    50   ~ 0
CMD_P
Text Label 4000 5100 0    50   ~ 0
GND
Text Label 4000 5200 0    50   ~ 0
LVDS0_Chip3_P
Text Label 4000 5300 0    50   ~ 0
LVDS0_Chip3_N
Text Label 4000 5400 0    50   ~ 0
MUX_Chip4
Text Notes 6100 3950 0    50   ~ 0
GND_S is connected to make the pigtail symmetric to GND_C\n(it shouldn’t be connected on the data adapter card
$EndSCHEMATC
